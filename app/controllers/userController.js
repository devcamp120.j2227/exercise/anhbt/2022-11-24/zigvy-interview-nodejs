const mongoose = require("mongoose");

const userModel = require("../models/userModel");
const createUser = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest);

    // B3: Thao tác với cơ sở dữ liệu
    let newUser = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        username: bodyRequest.username,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company: bodyRequest.company
    }

    userModel.create(newUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: User created",
                data: data
            })
        }
    })
}

const getAllUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get users success",
                    data: data
                })
            }
        });
}

const getUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    })
}

const updateUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updatedUser = {
        name: bodyRequest.name,
        username: bodyRequest.username,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company: bodyRequest.company
    }

    userModel.findByIdAndUpdate(userId, updatedUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update user success",
                old: data,
                new: updatedUser
            })
        }
    })
}

const deleteUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete user success"
            })
        }
    })
}

// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUserById: updateUserById,
    deleteUserById: deleteUserById
}