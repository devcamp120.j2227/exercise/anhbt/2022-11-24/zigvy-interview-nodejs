const mongoose = require("mongoose");

const commentModel = require("../models/commentModel");
const createComment = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest);

    // B3: Thao tác với cơ sở dữ liệu
    let newComment = {
        _id: mongoose.Types.ObjectId(),
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body,
    }

    commentModel.create(newComment, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Comment created",
                data: data
            })
        }
    })
}

const getAllComment = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.query.postId;
    let condition = {};
    if (postId) {
        condition.postId = postId;
    }
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    commentModel.find(condition).exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get comments success",
                    data: data
                })
            }
        });
}

const getCommentById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let commentId = request.params.commentId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Comment ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    commentModel.findById(commentId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get comment success",
                data: data
            })
        }
    })
}

const updateCommentById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let commentId = request.params.commentId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Comment ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updatedComment = {
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body,
    }

    commentModel.findByIdAndUpdate(commentId, updatedComment, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update comment success",
                old: data,
                new: updatedComment
            })
        }
    })
}

const deleteCommentById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let commentId = request.params.commentId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Comment ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    commentModel.findByIdAndDelete(commentId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete comment success"
            })
        }
    })
}
// khai báo model post 
const getCommentsOfPosts = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.params.postId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    commentModel.find({ postId : postId })
    .exec( (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get comment success",
                data: data
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createComment: createComment,
    getAllComment: getAllComment,
    getCommentById: getCommentById,
    updateCommentById: updateCommentById,
    deleteCommentById: deleteCommentById,
    getCommentsOfPosts: getCommentsOfPosts
}