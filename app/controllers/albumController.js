const mongoose = require("mongoose");

const albumModel = require("../models/albumModel");
const createAlbum = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest);

    // B3: Thao tác với cơ sở dữ liệu
    let newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title
    }

    albumModel.create(newAlbum, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Album created",
                data: data
            })
        }
    })
}

const getAllAlbum = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.query.userId;
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    albumModel.find(condition).exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get albums success",
                    data: data
                })
            }
        });
}

const getAlbumById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let albumId = request.params.albumId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    albumModel.findById(albumId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get album success",
                data: data
            })
        }
    })
}

const updateAlbumById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let albumId = request.params.albumId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updatedAlbum = {
        userId: bodyRequest.userId,
        title: bodyRequest.title
    }

    albumModel.findByIdAndUpdate(albumId, updatedAlbum, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update album success",
                old: data,
                new: updatedAlbum
            })
        }
    })
}

const deleteAlbumById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let albumId = request.params.albumId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    albumModel.findByIdAndDelete(albumId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete album success"
            })
        }
    })
}
// khai báo model user 
const getAlbumsOfUsers = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    albumModel.find({ userId : userId })
    .exec( (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get album success",
                data: data
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createAlbum: createAlbum,
    getAllAlbum: getAllAlbum,
    getAlbumById: getAlbumById,
    updateAlbumById: updateAlbumById,
    deleteAlbumById: deleteAlbumById,
    getAlbumsOfUsers: getAlbumsOfUsers
}