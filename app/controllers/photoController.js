const mongoose = require("mongoose");

const photoModel = require("../models/photoModel");
const createPhoto = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest);

    // B3: Thao tác với cơ sở dữ liệu
    let newPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId: bodyRequest.albumId,
        title: bodyRequest.title,
        url: bodyRequest.url,
        thumbnailUrl: bodyRequest.thumbnailUrl
    }

    photoModel.create(newPhoto, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Photo created",
                data: data
            })
        }
    })
}

const getAllPhoto = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let albumId = request.query.albumId;
    let condition = {};
    if (albumId) {
        condition.albumId = albumId;
    }
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    photoModel.find(condition).exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get photos success",
                    data: data
                })
            }
        });
}

const getPhotoById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let photoId = request.params.photoId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Photo ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    photoModel.findById(photoId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get photo success",
                data: data
            })
        }
    })
}

const updatePhotoById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let photoId = request.params.photoId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Photo ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updatedPhoto = {
        albumId: bodyRequest.albumId,
        title: bodyRequest.title,
        url: bodyRequest.url,
        thumbnailUrl: bodyRequest.thumbnailUrl
    }

    photoModel.findByIdAndUpdate(photoId, updatedPhoto, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update photo success",
                old: data,
                new: updatedPhoto
            })
        }
    })
}

const deletePhotoById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let photoId = request.params.photoId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Photo ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    photoModel.findByIdAndDelete(photoId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete photo success"
            })
        }
    })
}
// khai báo model album 
const getPhotosOfAlbums = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let albumId = request.params.albumId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    photoModel.find({ albumId : albumId })
    .exec( (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get photo success",
                data: data
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createPhoto: createPhoto,
    getAllPhoto: getAllPhoto,
    getPhotoById: getPhotoById,
    updatePhotoById: updatePhotoById,
    deletePhotoById: deletePhotoById,
    getPhotosOfAlbums: getPhotosOfAlbums
}