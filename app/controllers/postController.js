const mongoose = require("mongoose");

const postModel = require("../models/postModel");
const createPost = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest);

    // B3: Thao tác với cơ sở dữ liệu
    let newPost = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body,
    }

    postModel.create(newPost, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Post created",
                data: data
            })
        }
    })
}

const getAllPost = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.query.userId;
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    postModel.find(condition).exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get posts success",
                    data: data
                })
            }
        });
}

const getPostById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.params.postId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    postModel.findById(postId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get post success",
                data: data
            })
        }
    })
}

const updatePostById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.params.postId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updatedPost = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body,
    }

    postModel.findByIdAndUpdate(postId, updatedPost, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update post success",
                old: data,
                new: updatedPost
            })
        }
    })
}

const deletePostById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let postId = request.params.postId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Post ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    postModel.findByIdAndDelete(postId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete post success"
            })
        }
    })
}
// khai báo model user 
const getPostsOfUsers = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    postModel.find({ userId : userId })
    .exec( (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get post success",
                data: data
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createPost: createPost,
    getAllPost: getAllPost,
    getPostById: getPostById,
    updatePostById: updatePostById,
    deletePostById: deletePostById,
    getPostsOfUsers: getPostsOfUsers
}