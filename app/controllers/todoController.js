const mongoose = require("mongoose");

const todoModel = require("../models/todoModel");
const createTodo = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest);

    // B3: Thao tác với cơ sở dữ liệu
    let newTodo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        completed: bodyRequest.completed
    }

    todoModel.create(newTodo, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Todo created",
                data: data
            })
        }
    })
}

const getAllTodo = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.query.userId;
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    todoModel.find(condition).exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get todos success",
                    data: data
                })
            }
        });
}

const getTodoById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let todoId = request.params.todoId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Todo ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    todoModel.findById(todoId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get todo success",
                data: data
            })
        }
    })
}

const updateTodoById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let todoId = request.params.todoId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Todo ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updatedTodo = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        completed: bodyRequest.completed
    }

    todoModel.findByIdAndUpdate(todoId, updatedTodo, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update todo success",
                old: data,
                new: updatedTodo
            })
        }
    })
}

const deleteTodoById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let todoId = request.params.todoId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Todo ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    todoModel.findByIdAndDelete(todoId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete todo success"
            })
        }
    })
}
// khai báo model user 
const getTodosOfUsers = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    todoModel.find({ userId : userId })
    .exec( (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get todo success",
                data: data
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createTodo: createTodo,
    getAllTodo: getAllTodo,
    getTodoById: getTodoById,
    updateTodoById: updateTodoById,
    deleteTodoById: deleteTodoById,
    getTodosOfUsers: getTodosOfUsers
}