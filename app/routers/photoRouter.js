// import thư viện express
const express = require("express");
const Router = express.Router();

// khai báo controller
const photoController = require("../controllers/photoController")

Router.photo("/photos", photoController.createPhoto);

Router.get("/photos", photoController.getAllPhoto);
Router.get("/photos/:photoId", photoController.getPhotoById);

Router.put("/photos/:photoId", photoController.updatePhotoById);

Router.delete("/photos/:photoId", photoController.deletePhotoById);

Router.get("/albums/:albumId/photos", photoController.getPhotosOfAlbums)

module.exports = Router;


