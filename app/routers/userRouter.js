// import thư viện express
const express = require("express");
const Router = express.Router();
const userController = require("../controllers/userController")

Router.post("/users", userController.createUser);

Router.get("/users", userController.getAllUser);
Router.get("/users/:userId", userController.getUserById);

Router.put("/users/:userId", userController.updateUserById);

Router.delete("/users/:userId", userController.deleteUserById);

module.exports = Router;


