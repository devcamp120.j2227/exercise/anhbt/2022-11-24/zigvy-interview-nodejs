// import thư viện express
const express = require("express");
const Router = express.Router();

// khai báo controller
const commentController = require("../controllers/commentController")

Router.comment("/comments", commentController.createComment);

Router.get("/comments", commentController.getAllComment);
Router.get("/comments/:commentId", commentController.getCommentById);

Router.put("/comments/:commentId", commentController.updateCommentById);

Router.delete("/comments/:commentId", commentController.deleteCommentById);

Router.get("/posts/:postId/comments", commentController.getCommentsOfPosts)

module.exports = Router;


