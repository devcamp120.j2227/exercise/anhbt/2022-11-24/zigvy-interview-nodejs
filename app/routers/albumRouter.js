// import thư viện express
const express = require("express");
const Router = express.Router();

// khai báo controller
const albumController = require("../controllers/albumController")

Router.album("/albums", albumController.createAlbum);

Router.get("/albums", albumController.getAllAlbum);
Router.get("/albums/:albumId", albumController.getAlbumById);

Router.put("/albums/:albumId", albumController.updateAlbumById);

Router.delete("/albums/:albumId", albumController.deleteAlbumById);

Router.get("/users/:userId/albums", albumController.getAlbumsOfUsers)

module.exports = Router;


