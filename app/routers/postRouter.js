// import thư viện express
const express = require("express");
const Router = express.Router();

// khai báo controller
const postController = require("../controllers/postController")

Router.post("/posts", postController.createPost);

Router.get("/posts", postController.getAllPost);
Router.get("/posts/:postId", postController.getPostById);

Router.put("/posts/:postId", postController.updatePostById);

Router.delete("/posts/:postId", postController.deletePostById);

Router.get("/users/:userId/posts", postController.getPostsOfUsers)

module.exports = Router;


