// import thư viện express
const express = require("express");
const Router = express.Router();

// khai báo controller
const todoController = require("../controllers/todoController")

Router.todo("/todos", todoController.createTodo);

Router.get("/todos", todoController.getAllTodo);
Router.get("/todos/:todoId", todoController.getTodoById);

Router.put("/todos/:todoId", todoController.updateTodoById);

Router.delete("/todos/:todoId", todoController.deleteTodoById);

Router.get("/users/:userId/todos", todoController.getTodosOfUsers)

module.exports = Router;


