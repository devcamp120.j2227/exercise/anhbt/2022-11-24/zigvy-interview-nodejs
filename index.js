// import thư viện cần dùng
const express = require("express");
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();
// Khai báo middleware đọc json
app.use(express.json());
// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

// import routers
const userRouter = require("./app/routers/userRouter");
const postRouter = require("./app/routers/postRouter");
const commentRouter = require("./app/routers/commentRouter");
const albumRouter = require("./app/routers/albumRouter");
const photoRouter = require("./app/routers/photoRouter");
const todoRouter = require("./app/routers/todoRouter");
// declare routers
app.use(userRouter);
app.use(postRouter);
app.use(commentRouter);
app.use(albumRouter);
app.use(photoRouter);
app.use(todoRouter);

// kết nối csdl
mongoose.connect("mongodb://127.0.0.1:27017/Zigvy_Interview", (err) => {
    if(err) { throw err }
    console.log("Connect to MongoDB successfully!");
})

// middleware log thời gian và req method
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)

// Khai báo API dạng get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

// Khai báo cổng của project
const port = 8000;
// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})